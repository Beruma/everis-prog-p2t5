package dam.prog.p2;
/*
 * Sustituir en la actividad anterior el uso de arrays por ficheros y cambiar la funcionalidad necesaria para que
 * todo se haga a través de ficheros.
 */

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Beatriz
 */
public class Ficheros {

    static ArrayList<Cliente> lista_clientes = new ArrayList<>();
    static ArrayList<Articulo> lista_articulos = new ArrayList<>();
    static String usuario;
    static String dni;
    static Scanner key = new Scanner(System.in);

    public static void main(String[] args) {
        arrancar();
        boolean salir = false;
        int opcion;
        while (!salir) {
            System.out.println("1. Comprar");
            System.out.println("2. Registrar Cliente");
            System.out.println("3. Salir");
            opcion = key.nextInt();

            switch (opcion) {
                case 1:
                    comprobarCliente();
                    break;
                case 2:
                    registrarCliente();
                    break;
                case 3:
                    salir = true;
                    break;
                default:
                    System.out.println("No esiste esa opción");
            }
        }
    }

    //comprueba si el cliente esta registrado buscandolo en LISTACLIENTES
    public static void comprobarCliente() {

        System.out.println("Introduzca nombre de usuario");
        usuario = key.nextLine();
        System.out.println("Introduzca su DNI");
        dni = key.nextLine();

        for (Cliente cliente : lista_clientes) {
            if ((cliente.getUsuario().equalsIgnoreCase(usuario)) && (cliente.getDni().equalsIgnoreCase(dni))) {
                comprarArticulos();
            } else {
                registrarCliente();
            }
        }

    }

    //Aquí el cliente podrá elegir el artículo que desea comprar y la cantidad//
    public static void comprarArticulos() {

        boolean siNo = false;

        while (!siNo) {

            System.out.println("Elija el artículo");
            mostrarArticulos();
            int codigoArt = key.nextInt();

            if (comprobarSiExisteArticulo(codigoArt)) {
                System.out.println("¿Cuantos?");
                int cantidad = key.nextInt();

                if (comprobarSaldo(codigoArt, cantidad)) {
                    System.out.println("¿Desea seguir comprando?");
                    String respuesta = key.nextLine();
                    if (respuesta.equalsIgnoreCase("si")) {
                        siNo = false;
                    } else {
                        mostrarRecibosCompras();
                        siNo = true;
                    }
                }
            }
        }
    }

    //Muestra los artículos en venta//
    public static void mostrarArticulos() {
        for (int i = 0; i < lista_articulos.size(); i++) {
            System.out.println(lista_articulos.get(i));
        }
    }

    //Registra a un nuevo cliente y le crea un fichero//
    public static void registrarCliente() {
        Cliente cliente = new Cliente();
        Scanner key = new Scanner(System.in);
        System.out.println("Introduzca su Usuario");
        usuario = key.nextLine();
        cliente.setUsuario(usuario);
        System.out.println("Introduzca su DNI");
        dni = key.nextLine();
        cliente.setDni(dni);
        System.out.println("Introduzca su password solo números");
        int password = key.nextInt();
        cliente.setPassword(password);
        System.out.println("Introduzca su saldo");
        double saldo = key.nextDouble();
        cliente.setSaldo(saldo);
        lista_clientes.add(cliente);


        try {
            FileOutputStream fos = new FileOutputStream(new File("./ficheros/" + usuario + dni + ".dat"));
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            bos.write(("Nombre Cliente: " + usuario).getBytes());
            bos.write(" ".getBytes());
            bos.write(("dni: " + dni).getBytes());
            bos.write("\n".getBytes());
            bos.flush();
            bos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        grabarClientes();
    }

    //Comprueba si existe el articulo seleccionado//
    public static boolean comprobarSiExisteArticulo(int codigoArt) {
        for (Articulo articulo : lista_articulos) {
            if (articulo.getCodigo() == codigoArt) {
                return true;
            } else {

                System.out.println("el artículo no se encuentra");
            }
        }
        return false;
    }

    //Comprueba si tiene saldo suficiente para comprar//
    public static boolean comprobarSaldo(int codigoArt, int cantidad) {
        double precioTotal = 0;
        Articulo articulo = new Articulo();
        for (int i = 0; i < lista_articulos.size(); i++) {
            if (articulo.getCodigo() == codigoArt) {
                precioTotal = articulo.getPrecio() * cantidad;
            }
        }
        for (Cliente cliente : lista_clientes) {
            if (cliente.getDni().equalsIgnoreCase(dni)) {
                if (cliente.getSaldo() > precioTotal) {
                    double saldoCliente = cliente.getSaldo() - precioTotal;
                    cliente.setSaldo(saldoCliente);
                    escribirRecibo(articulo, codigoArt, cantidad, precioTotal);
                    return true;
                }
            }
        }
        return false;
    }

    //Escribe en el fichero del cliente los recibos de compra//
    public static void escribirRecibo(Articulo articulo, int codArt, int cantidad, double precioTotal) {

        File fichero = new File("./ficheros/" + usuario + dni + ".dat");
        try {
            FileOutputStream fos = new FileOutputStream(fichero, true);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            bos.write((codArt + ":" + articulo.getDescripcion()).getBytes());
            bos.write("   ".getBytes());
            bos.write((codArt + ":" + cantidad).getBytes());
            bos.write("   ".getBytes());
            bos.write((codArt + ": " + articulo.getPrecio()).getBytes());
            bos.write("   ".getBytes());
            bos.write((codArt + ": " + precioTotal).getBytes());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Muestra al cliente todos los recibos de compra//
    public static void mostrarRecibosCompras() {
        try {
            FileInputStream fis = new FileInputStream(new File("./ficheros/" + usuario + dni + ".dat"));
            BufferedInputStream bis = new BufferedInputStream(fis);
            int ch;
            while ((ch = bis.read()) != -1) {
                System.out.println((char) ch);
            }
            bis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Lee los datos en los respectivos ficheros y los introduce dentro de los respectivos arrays
    public static void arrancar() {

        File f = new File("./listas/Articulos.dat");

        if (f.exists()) {
            try {
                ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(f));
                lista_articulos = (ArrayList<Articulo>) objectInputStream.readObject();
                objectInputStream.close();
            } catch (ClassNotFoundException i) {
                System.out.println("No existe el archivo");
            } catch (IOException e) {
                System.out.println("Fin del archivo o archivo vacio");
            }

        }
        File fC = new File("./listas/Clientes.dat");

        if (fC.exists()) {
            try {
                ObjectInputStream objectInputStream2 = new ObjectInputStream(new FileInputStream(fC));
                lista_clientes = (ArrayList<Cliente>) objectInputStream2.readObject();
                objectInputStream2.close();
            } catch (ClassNotFoundException i) {
                System.out.println("No existe el archivo");
            } catch (IOException e) {
                System.out.println("Fin del archivo o archivo vacio");
            }
        }
    }


    //Escribe un cliente nuevo en el fichero de clientes//
    public static void grabarClientes() {
        //Escribo en el archivo los objetos persona que se encuentran en el arrayList con los datos de los usuarios
        File fichero = new File("./listas/Clientes.dat");
        try {
            FileOutputStream fos = new FileOutputStream(fichero, true);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            for (Cliente a : lista_clientes) {
                bos.write(a.toString().getBytes());
                bos.write("\n".getBytes());
                bos.flush();
            }
            bos.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}