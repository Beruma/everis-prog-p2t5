package dam.prog.p2;

/**
 * @author Beatriz
 */
public class Cliente {
    String dni;
    String usuario;
    int password;
    double saldo;

    public Cliente() {
    }

    public Cliente(String dni, String usuario, int password, double saldo) {
        this.dni = dni;
        this.usuario = usuario;
        this.password = password;
        this.saldo = saldo;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "dni='" + dni + '\'' +
                ", usuario='" + usuario + '\'' +
                ", password=" + password +
                ", saldo=" + saldo +
                '}';
    }
}